package ru.ekfedorov.tm.listener.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.event.ConsoleEvent;
import ru.ekfedorov.tm.listener.AbstractProjectListener;
import ru.ekfedorov.tm.endpoint.Session;
import ru.ekfedorov.tm.exception.system.NullObjectException;

@Component
public final class ProjectClearAllListener extends AbstractProjectListener {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Clear all project.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-clear";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@projectClearAllListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService == null) throw new NullObjectException();
        @Nullable final Session session = sessionService.getSession();
        System.out.println("[CLEAR]");
        adminEndpoint.clearProject(session);
        System.out.println("--- successfully cleared ---\n");
    }

}
