package ru.ekfedorov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import ru.ekfedorov.tm.api.service.IPropertyService;

@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @NotNull
    private final Environment environment;

    @NotNull
    public static final String APPLICATION_VERSION = "version";

    @NotNull
    public static final String APPLICATION_VERSION_DEFAULT = "1.0.0";

    @NotNull
    public static final String DEVELOPER_EMAIL = "email";

    @NotNull
    public static final String DEVELOPER_EMAIL_DEFAULT = "";

    @NotNull
    public static final String DEVELOPER_NAME = "developer";

    @NotNull
    public static final String DEVELOPER_NAME_DEFAULT = "";

    @NotNull
    public static final String PASSWORD_ITERATION = "password.iteration";

    @NotNull
    public static final String PASSWORD_ITERATION_DEFAULT = "1";

    @NotNull
    public static final String PASSWORD_SECRET = "password.secret";

    @NotNull
    public static final String PASSWORD_SECRET_DEFAULT = "";

    @NotNull
    public static final String SIGN_ITERATION = "sign.iteration";

    @NotNull
    public static final String SIGN_ITERATION_DEFAULT = "1";

    @NotNull
    public static final String SIGN_SECRET = "sign.secret";

    @NotNull
    public static final String SIGN_SECRET_DEFAULT = "";

    @SneakyThrows
    public PropertyService(@NotNull final Environment environment) {
        this.environment = environment;
    }

    @Override
    public @NotNull String getApplicationVersion() {
        @Nullable final String version;
        if (Manifests.exists(APPLICATION_VERSION)) version = Manifests.read(APPLICATION_VERSION);
        else version = environment.getProperty(APPLICATION_VERSION, APPLICATION_VERSION_DEFAULT);
        return version;
    }

    @Override
    public @NotNull String getDeveloperEmail() {
        @Nullable String email;
        if (Manifests.exists(DEVELOPER_EMAIL)) email = Manifests.read(DEVELOPER_EMAIL);
        else email = environment.getProperty(DEVELOPER_EMAIL, DEVELOPER_EMAIL_DEFAULT);
        return email;
    }

    @Override
    public @NotNull String getDeveloperName() {
        @Nullable String developer;
        if (Manifests.exists(DEVELOPER_NAME)) developer = Manifests.read(DEVELOPER_NAME);
        else developer = environment.getProperty(DEVELOPER_NAME, DEVELOPER_NAME_DEFAULT);
        return developer;
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        @NotNull final String value = environment.getProperty(PASSWORD_ITERATION, PASSWORD_ITERATION_DEFAULT);
        return Integer.valueOf(value);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return environment.getProperty(PASSWORD_SECRET, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getSignIteration() {
        @NotNull final String value = environment.getProperty(SIGN_ITERATION, SIGN_ITERATION_DEFAULT);
        return Integer.valueOf(value);
    }

    @NotNull
    @Override
    public String getSignSecret() {
        return environment.getProperty(SIGN_SECRET, SIGN_SECRET_DEFAULT);
    }

}
