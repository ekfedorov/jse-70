package ru.ekfedorov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.ekfedorov.tm.api.endpoint.EndpointLocator;
import ru.ekfedorov.tm.bootstrap.Bootstrap;
import ru.ekfedorov.tm.cofiguration.ClientConfiguration;
import ru.ekfedorov.tm.marker.IntegrationCategory;

public class UserEndpointTest {

    @NotNull
    public final AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ClientConfiguration.class);

    @NotNull
    private final SessionEndpoint sessionEndpoint = context.getBean(SessionEndpoint.class);

    @NotNull
    private final UserEndpoint userEndpoint = context.getBean(UserEndpoint.class);

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = context.getBean(AdminUserEndpoint.class);

    private Session session;

    private Session sessionAdmin;

    @Before
    @SneakyThrows
    public void before() {
        session = sessionEndpoint.openSession("test", "test");
        sessionAdmin = sessionEndpoint.openSession("admin", "admin");
    }

    @After
    @SneakyThrows
    public void after() {
        sessionEndpoint.closeSession(session);
        sessionEndpoint.closeSession(sessionAdmin);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void createUserTest() {
        adminUserEndpoint.removeByLogin(sessionAdmin, "test2");
        userEndpoint.createUser("test2", "test2", "test@twst.ru");
        Assert.assertNotNull(userEndpoint.findUserByLogin(session, "test2"));
        adminUserEndpoint.removeByLogin(sessionAdmin, "test2");
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findUserByLoginTest() {
        adminUserEndpoint.removeByLogin(sessionAdmin, "test2");
        userEndpoint.createUser("test2", "test2", "test@twst.ru");
        Assert.assertNotNull(userEndpoint.findUserByLogin(session, "test2"));
        adminUserEndpoint.removeByLogin(sessionAdmin, "test2");
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findUserOneBySessionTest() {
        final String login = userEndpoint.findUserOneBySession(session).getLogin();
        Assert.assertEquals("test", login);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void setPasswordTest() {
        final String passwordNew = "newTestPass";
        userEndpoint.setPassword(session, passwordNew);
        final Session session2 = sessionEndpoint.openSession("test", passwordNew);
        Assert.assertNotNull(session2);
        userEndpoint.setPassword(session2, "test");
        sessionEndpoint.closeSession(session2);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void updateUserTest() {
        final User user_before = userEndpoint.findUserOneBySession(session);
        final String firstName_before = user_before.getFirstName();
        final String lastName_before = user_before.getLastName();
        final String middleName_before = user_before.getMiddleName();
        final String firstName = "fName";
        final String lastName = "lName";
        final String middleName = "mName";
        userEndpoint.updateUser(session, firstName, lastName, middleName);
        final User user = userEndpoint.findUserOneBySession(session);
        Assert.assertEquals(firstName, user.getFirstName());
        Assert.assertEquals(lastName, user.getLastName());
        Assert.assertEquals(middleName, user.getMiddleName());
        userEndpoint.updateUser(session, firstName_before, lastName_before, middleName_before);
    }


}
