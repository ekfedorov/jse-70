package ru.ekfedorov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.ekfedorov.tm.cofiguration.ClientConfiguration;
import ru.ekfedorov.tm.marker.IntegrationCategory;

public class SessionEndpointTest {

    @NotNull
    public final AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ClientConfiguration.class);

    @NotNull
    private final SessionEndpoint sessionEndpoint = context.getBean(SessionEndpoint.class);

    @NotNull
    private final TaskEndpoint taskEndpoint = context.getBean(TaskEndpoint.class);

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void openSession() {
        final Session session = sessionEndpoint.openSession("test", "test");
        Assert.assertNotNull(session);
        sessionEndpoint.closeSession(session);
    }

//    @Test(expected = AccessDeniedException_Exception.class)
//    @SneakyThrows
//    @Category(IntegrationCategory.class)
//    public void closeSession() {
//        final Session session = sessionEndpoint.openSession("test", "test");
//        sessionEndpoint.closeSession(session);
//        taskEndpoint.findAllTask(session);
//    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void closeSession2() {
        final Session session = sessionEndpoint.openSession("test", "test");
        sessionEndpoint.closeSession(session);
    }

}
