package ru.ekfedorov.tm.exception.system;

import ru.ekfedorov.tm.exception.AbstractException;

public final class NullObjectException extends AbstractException {

    public NullObjectException() {
        super("Error! Object is null...");
    }

}
