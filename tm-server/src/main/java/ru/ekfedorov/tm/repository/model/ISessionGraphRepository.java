package ru.ekfedorov.tm.repository.model;

import ru.ekfedorov.tm.api.repository.IGraphRepository;
import ru.ekfedorov.tm.model.SessionGraph;

public interface ISessionGraphRepository extends IGraphRepository<SessionGraph> {
}
