package ru.ekfedorov.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ekfedorov.tm.dto.AbstractEntity;

public interface IRepository<E extends AbstractEntity> extends JpaRepository<E, String> {
}
