<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<style>@import url("/styles/main.css");</style>
<head>
  <title>${view_name}</title>
</head>
<body>
<table style="width: 100%">
  <tr>
    <td class="head">
      <div>TASK MANAGER</div>
    </td>
    <td class="mini">
      <a href="/tasks">TASKS</a>
    </td>
    <td class="mini">
      <a href="/projects">PROJECTS</a>
    </td>
    <td class="mini">
    <sec:authorize access="isAuthenticated()">
                     <a href="/logout">Logout</a>
                </sec:authorize>
                <sec:authorize access="!isAuthenticated()">
                     <a href="/login">Login</a>
                </sec:authorize>
     </td>
  </tr>
</table>
<table class="upper-head">
  <tr>
    <td>
      <div>${view_name}</div>
    </td>
  </tr>
</table>
