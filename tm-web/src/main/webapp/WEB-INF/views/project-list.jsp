<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="_header.jsp"/>
    <table class="list">
        <tr>
            <th>ID</th>
            <th>NAME</th>
            <th>STATUS</th>
            <th>DATE FINISH</th>
            <th>DESCRIPTION</th>
            <th class="mini">EDIT</th>
            <th class="mini">DELETE</th>
        </tr>
        <c:forEach var="project" items="${projects}">
            <tr>
                <td>
                    <c:out value="${project.id}"/>
                </td>
                <td>
                    <c:out value="${project.name}"/>
                </td>
                <td>
                    <c:out value="${project.status.getDisplayName()}"/>
                </td>
                <td>
                    <fmt:formatDate value="${project.dateFinish}" pattern="dd.MM.yyyy"/>
                </td>
                <td>
                    <c:out value="${project.description}"/>
                </td>
                <td class="mini">
                    <a href="/project/edit/${project.id}">EDIT</a>
                </td>
                <td class="mini">
                    <a href="/project/delete/${project.id}">DELETE</a>
                </td>
            </tr>
        </c:forEach>
    </table>
    <table class="create">
        <tr>
            <td>
                <form action="/project/create">
                    <button>CREATE</button>
                </form>
            </td>
        </tr>
    </table>
</body>
</html>
