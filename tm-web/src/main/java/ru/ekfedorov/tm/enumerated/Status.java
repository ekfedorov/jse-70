package ru.ekfedorov.tm.enumerated;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETE("Complete");

    private final String displayName;

    Status(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public static Status getStatus(final String status) {
        if(status.equals(COMPLETE.displayName)) return COMPLETE;
        if(status.equals(IN_PROGRESS.displayName)) return IN_PROGRESS;
        return NOT_STARTED;
    }

}
