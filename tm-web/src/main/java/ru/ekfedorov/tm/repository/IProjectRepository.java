package ru.ekfedorov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.ekfedorov.tm.model.Project;

import java.util.List;


public interface IProjectRepository extends JpaRepository<Project, String> {

    Project findByUserIdAndId(final String userId, final String id);

    void deleteByUserId(final String userId);

    void deleteByUserIdAndId(final String userId, final String id);

    List<Project> findAllByUserId(final String userId);

    Project findByUserIdAndName(final String userId, final String name);

    @Query("SELECT e FROM Project e WHERE e.userId = :userId")
    Project findByIndexAndUserId(final String userId, final int index);

    void deleteByUserIdAndName(final String userId, final String name);

}
