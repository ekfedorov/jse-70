package ru.ekfedorov.tm.config;

import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import ru.ekfedorov.tm.endpoint.AuthEndpoint;
import ru.ekfedorov.tm.endpoint.ProjectEndpoint;
import ru.ekfedorov.tm.endpoint.TaskEndpoint;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.xml.ws.Endpoint;

@Configuration
@ComponentScan("ru.ekfedorov.tm")
public class ApplicationConfiguration implements WebMvcConfigurer, WebApplicationInitializer {

    @Bean
    public ViewResolver internalResourceViewResolver() {
        InternalResourceViewResolver bean = new InternalResourceViewResolver();
        bean.setViewClass(JstlView.class);
        bean.setPrefix("/WEB-INF/views/");
        bean.setSuffix(".jsp");
        return bean;
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
                .addResourceHandler("/**")
                .addResourceLocations("/")
                .setCachePeriod(31556926);
    }

    @Bean
    public SpringBus cxf() {
        return new SpringBus();
    }

    @Override
    public void onStartup(@NotNull final ServletContext servletContext)
            throws ServletException {
        @NotNull final CXFServlet cxfServlet = new CXFServlet();
        @NotNull final ServletRegistration.Dynamic dynamic =
                servletContext.addServlet("cxfServlet", cxfServlet);
        dynamic.addMapping("/ws/*");
        dynamic.setLoadOnStartup(1);
    }

    @Bean
    public Endpoint projectEndpointRegistry(
            @NotNull final ProjectEndpoint projectEndpoint,
            @NotNull final SpringBus cxf
    ) {
        @NotNull final Endpoint endpoint =
                new EndpointImpl(cxf, projectEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpointRegistry(
            @NotNull final TaskEndpoint taskEndpoint,
            @NotNull final SpringBus cxf
    ) {
        @NotNull final Endpoint endpoint =
                new EndpointImpl(cxf, taskEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint authEndpointRegistry(
            @NotNull final AuthEndpoint authEndpoint,
            @NotNull final SpringBus cxf
    ) {
        @NotNull final Endpoint endpoint =
                new EndpointImpl(cxf, authEndpoint);
        endpoint.publish("/AuthEndpoint");
        return endpoint;
    }

}
