package ru.ekfedorov.tm.exception;

import org.jetbrains.annotations.NotNull;

public class EmptyLoginException extends AbstractException {

    @NotNull
    public EmptyLoginException() {
        super("Error. Login is empty");
    }

}
