package ru.ekfedorov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.enumerated.RoleType;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@Setter
@Getter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "app_role")
@XmlAccessorType(XmlAccessType.FIELD)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Role extends AbstractEntity {

    @NotNull
    @Enumerated(EnumType.STRING)
    private RoleType role = RoleType.USER;

    @Override
    public String toString() {
        return role.toString();
    }
}
