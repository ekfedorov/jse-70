package ru.ekfedorov.tm.api.endpoint;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.ekfedorov.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @WebMethod
    @GetMapping("/findAll")
    List<Task> findAll();

    @WebMethod
    @GetMapping("/find/{id}")
    Task find(@PathVariable("id") String id);

    @WebMethod
    @PostMapping("/create")
    Task create(@RequestBody Task task);

    @WebMethod
    @PostMapping("/createAll")
    List<Task> createAll(@RequestBody List<Task> tasks);

    @WebMethod
    @PostMapping("/save")
    Task save(@RequestBody Task task);

    @WebMethod
    @PostMapping("/saveAll")
    List<Task> saveAll(@RequestBody List<Task> tasks);

    @WebMethod
    @PostMapping("/delete/{id}")
    void delete(@PathVariable("id") String id);

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll();

}
