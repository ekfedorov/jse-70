package ru.ekfedorov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.ekfedorov.tm.api.endpoint.IProjectEndpoint;
import ru.ekfedorov.tm.api.service.IProjectService;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService
@RestController
@RequestMapping("/api/projects")
public class ProjectEndpoint implements IProjectEndpoint {

    @Autowired
    private IProjectService service;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return new ArrayList<>(service.findAll(UserUtil.getUserId()));
    }

    @Override
    @WebMethod
    @GetMapping("/find/{id}")
    public Project find(@PathVariable("id") @WebParam(name = "id") final String id) {
        return service.findById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping("/create")
    public Project create(@RequestBody @WebParam(name = "project") final Project project) {
        service.add(UserUtil.getUserId(), project);
        return project;
    }

    @Override
    @WebMethod
    @PostMapping("/createAll")
    public List<Project> createAll(@RequestBody @WebParam(name = "projects") final List<Project> projects) {
        service.addAll(UserUtil.getUserId(), projects);
        return projects;
    }

    @Override
    @WebMethod
    @PutMapping("/save")
    public Project save(@RequestBody @WebParam(name = "project") final Project project) {
        service.add(UserUtil.getUserId(), project);
        return project;
    }

    @Override
    @WebMethod
    @PutMapping("/saveAll")
    public List<Project> saveAll(@RequestBody @WebParam(name = "projects") final List<Project> projects) {
        service.addAll(UserUtil.getUserId(), projects);
        return projects;
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") @WebParam(name = "id") final String id) {
        service.removeById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteAll")
    public void deleteAll() {
        service.clear(UserUtil.getUserId());
    }

}
