package ru.ekfedorov.tm.client.soap;

import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.api.endpoint.IProjectEndpoint;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class ProjectClient {

    public static IProjectEndpoint getInstance(@NotNull final String baseURL)
            throws MalformedURLException {
        @NotNull final String wsdl = baseURL + "/ws/ProjectEndpoint?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final String lp = "ProjectEndpointService";
        @NotNull final String ns = "http://endpoint.tm.ekfedorov.ru/";
        @NotNull final QName name = new QName(ns, lp);
        @NotNull final IProjectEndpoint soap =
                Service.create(url, name).getPort(IProjectEndpoint.class);
        return soap;
    }

}
